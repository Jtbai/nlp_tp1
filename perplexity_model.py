class PerplexityModel:

    def __init__(self):
        self.default_probability = 0
        self.delta = 0

    def generate_probability(self,n_grams,n):
        raise NotImplemented()

    def generate_perplexity(self,language_model,n_grams_to_evaluate,n):

        probabilities = self.generate_probability(language_model.n_gram_model,n)
        perplexity = 1
        nb_counted = 0
        for ngram, count in n_grams_to_evaluate.items():
            n_gram_prob = probabilities.get(ngram, self.default_probability)
            if n_gram_prob > 0:
                perplexity += count * (1 / n_gram_prob)
                nb_counted += 1

        return perplexity ** (1 / nb_counted)

    def generate_chained_probability(self,n_gram_model,n):
        chained_probabilities = {}
        for n_gram, _ in n_gram_model[n].items():
            chained_probabilities[n_gram] = self._get_chained_probability(n_gram_model,n_gram)
        return chained_probabilities


    def _get_chained_probability(self, n_gram_model, n_gram):
        if len(n_gram) == 1:
            count_unigram = sum([x for x in n_gram_model[1].values()])+ (len(n_gram_model[1].values())*self.delta)
            return (n_gram_model[1].get(n_gram,0)+self.delta) / count_unigram
        else:
            return self._get_chained_probability(n_gram_model, n_gram[:-1]) * self._get_markov_probability(n_gram_model, n_gram[-2:])

    def _get_markov_probability(self, n_gram_model, bigram):
        count_first_letter = n_gram_model[1].get(bigram[0],0)+self.delta
        count_bigram = n_gram_model[2].get(bigram,0)+self.delta
        if count_first_letter >0:
            return count_bigram / count_first_letter
        else:
            return 0

class NoSmootingProbabilityModel(PerplexityModel):

    def generate_probability(self,n_grams,n):
        return self.generate_chained_probability(n_grams,n)

class LaPlaceSmoothingProbabilityModel(PerplexityModel):

    def __init__(self, delta):
        super(LaPlaceSmoothingProbabilityModel,self).__init__()
        self.delta = delta

    def generate_probability(self,n_grams,n):

        n_gram_probability = self.generate_chained_probability(n_grams,n)

        total_vocabulary_size = sum([x for x in n_grams[1].values()])
        self.default_probability = self.delta / total_vocabulary_size

        return n_gram_probability

class LinearSmoothing(PerplexityModel):


    def __init__(self, lambdas):
        super(LinearSmoothing,self).__init__()
        self.lambdas = lambdas

    def generate_perplexity(self,language_model,n_grams_to_evaluate,n):

        perplexity = 1
        nb_counted = 0
        for ngram, count in n_grams_to_evaluate.items():

            n_gram_prob = self.get_linear_probability(language_model.n_gram_model, ngram)
            if n_gram_prob > 0:
                perplexity += count * (1 / n_gram_prob)
                nb_counted += 1
        return perplexity ** (1 / nb_counted)


    def get_linear_probability(self,n_gram_model,n_gram_to_evaluate):
        if len(n_gram_to_evaluate) == 1:
            count_unigram = sum([x for x in n_gram_model[1].values()])
            return self.lambdas[1] * (n_gram_model[1].get(n_gram_to_evaluate,0)+self.delta) / count_unigram
        else:
            n_gram_degree = len(n_gram_to_evaluate)
            return self._get_chained_probability(n_gram_model,n_gram_to_evaluate) * self.lambdas[n_gram_degree] + self.get_linear_probability(n_gram_model,n_gram_to_evaluate[:-1])
