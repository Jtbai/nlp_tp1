from unittest import TestCase

from utils import convert_selection_string_to_field_file, get_full_file_text
from fields import *

HTML_DOCS_LOCATION = './data/factbook/fields'
PRINT_DOCS_PREFIX = 'print_'


class TestFields(TestCase):

    def test_national_anthem(self):
        field_file = convert_selection_string_to_field_file("NATIONAL_ANTHEM")
        html_doc = get_full_file_text(HTML_DOCS_LOCATION, field_file)

        national_anthem = NationalAnthem(html_doc)
        self.assertEqual('O Canada', national_anthem.get_value("canada"))

    def test_national_anthem_us(self):
        field_file = convert_selection_string_to_field_file("NATIONAL_ANTHEM")
        html_doc = get_full_file_text(HTML_DOCS_LOCATION, field_file)

        national_anthem = NationalAnthem(html_doc)
        self.assertEqual('The Star-Spangled Banner', national_anthem.get_value("united states"))

    def test_literacy(self):
        field_file = convert_selection_string_to_field_file("LITERACY")
        html_doc = get_full_file_text(HTML_DOCS_LOCATION, field_file)

        literacy = Literacy(html_doc)
        self.assertEqual('99%', literacy.get_value("canada"))

    def test_exports(self):
        field_file = convert_selection_string_to_field_file("EXPORTS")
        html_doc = get_full_file_text(HTML_DOCS_LOCATION, field_file)

        exports = Exports(html_doc)
        self.assertEqual('$12200000', exports.get_value("Anguilla"))

    def test_gdp_real_growth_rate(self):
        field_file = convert_selection_string_to_field_file("GDP_REAL_GROWTH_RATE")
        html_doc = get_full_file_text(HTML_DOCS_LOCATION, field_file)

        gdp_growth_rate = GdpRealGrowthRate(html_doc)
        self.assertEqual('3.5%', gdp_growth_rate.get_value("Argentina"))


    def test_gdp_per_capita(self):
        field_file = convert_selection_string_to_field_file("GDP_PER_CAPITA")
        html_doc = get_full_file_text(HTML_DOCS_LOCATION, field_file)

        national_anthem = GdpPerCapita(html_doc)
        self.assertEqual('$37900', national_anthem.get_value("Andorra"))

    def test_natural_hazard_no(self):
        field_file = convert_selection_string_to_field_file("NATURAL_HAZARDS")
        html_doc = get_full_file_text(HTML_DOCS_LOCATION, field_file)

        natural_hazards = NaturalHazards(html_doc)
        self.assertEqual('NO', natural_hazards.get_value("Angola"))

    def test_natural_hazard_yes(self):
        field_file = convert_selection_string_to_field_file("NATURAL_HAZARDS")
        html_doc = get_full_file_text(HTML_DOCS_LOCATION, field_file)

        natural_hazards = NaturalHazards(html_doc)
        self.assertEqual('YES', natural_hazards.get_value("Antarctica"))

    def test_executive_branch_no_interim(self):
        field_file = convert_selection_string_to_field_file("EXECUTIVE_BRANCH")
        html_doc = get_full_file_text(HTML_DOCS_LOCATION, field_file)

        executive_branch = ExecutiveBranch(html_doc)
        self.assertEqual('ELIZABETH II, Queen', executive_branch.get_value("canada"))


    def test_executive_branch_with_interim(self):
        field_file = convert_selection_string_to_field_file("EXECUTIVE_BRANCH")
        html_doc = get_full_file_text(HTML_DOCS_LOCATION, field_file)

        executive_branch = ExecutiveBranch(html_doc)
        self.assertEqual('Catherine SAMBA-PANZA, Interim President', executive_branch.get_value("Central African Republic"))
    #

    def test_diplomatic_with_full_address(self):
        field_file = convert_selection_string_to_field_file("DIPLOMATIC_REPRESENTATION_FROM_US")
        html_doc = get_full_file_text(HTML_DOCS_LOCATION, field_file)

        diplomatic_reprensentation = DiplomaticRepresentationFromUs(html_doc)
        self.assertEqual("9510 Tirana Place, Dulles", diplomatic_reprensentation.get_value("Albania"))


    def test_diplomatic_with_po_box(self):
        field_file = convert_selection_string_to_field_file("DIPLOMATIC_REPRESENTATION_FROM_US")
        html_doc = get_full_file_text(HTML_DOCS_LOCATION, field_file)

        diplomatic_reprensentation = DiplomaticRepresentationFromUs(html_doc)
        self.assertEqual("G. P. O. Box 323, Dhaka", diplomatic_reprensentation.get_value("Bangladesh"))


    def test_diplomatic_with_po_box_italy(self):
        field_file = convert_selection_string_to_field_file("DIPLOMATIC_REPRESENTATION_FROM_US")
        html_doc = get_full_file_text(HTML_DOCS_LOCATION, field_file)

        diplomatic_reprensentation = DiplomaticRepresentationFromUs(html_doc)
        self.assertEqual("PSC 59, Box 100, APO", diplomatic_reprensentation.get_value("Italy"))


    def test_diplomatic_with_po_box_nK(self):
        field_file = convert_selection_string_to_field_file("DIPLOMATIC_REPRESENTATION_FROM_US")
        html_doc = get_full_file_text(HTML_DOCS_LOCATION, field_file)

        diplomatic_reprensentation = DiplomaticRepresentationFromUs(html_doc)
        self.assertEqual("No_diplomatic_rep", diplomatic_reprensentation.get_value("Korea, North"))


    def test_diplomatic_with_po_box_senegal(self):
        field_file = convert_selection_string_to_field_file("DIPLOMATIC_REPRESENTATION_FROM_US")
        html_doc = get_full_file_text(HTML_DOCS_LOCATION, field_file)

        diplomatic_reprensentation = DiplomaticRepresentationFromUs(html_doc)
        self.assertEqual("B. P. 49, Dakar", diplomatic_reprensentation.get_value("Senegal"))

