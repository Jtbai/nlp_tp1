from unittest import TestCase
from language_model import LanguageModel
from perplexity_model import PerplexityModel, NoSmootingProbabilityModel

class TestLanguageClassifier(TestCase):

    language_model = None

    def test_when_entering_a_bad_language_raise_exception(self):
        with self.assertRaises(FileNotFoundError) as f:
            self.language_model = LanguageModel('asdfgdfg')

    def test_when_entering_a_good_languga_no_exception(self):
        LanguageModel.document_loader('english',"train")

    def test_when_adding_a_remove_function_to_the_preprocess_pipeline_the_text_is_removed(self):
        the_doc = "this document should not have the following caracter: !"
        pipeline = {'remove':[' not']}
        self.assertEqual(the_doc.replace(' not',''),LanguageModel.pre_process_document(the_doc,pipeline))

    def test_when_adding_a_lower_function_to_the_preprocess_pipeline_the_text_is_lowered(self):
        the_doc = "THIS DOCUMENT IS LOWER CASE"
        pipeline = {'to_lower': 1}
        self.assertEqual(the_doc.lower(), LanguageModel.pre_process_document(the_doc, pipeline))

    def test_when_genereting_a_n_gram_correct_n_gram_is_generated(self):
        the_doc = "thisdocumenth"
        n_grams = {'th':2,'hi':1,'is':1,'sd':1,'do':1,'oc':1,'cu':1,'um':1,'me':1,'en':1,'nt':1}

        self.assertDictEqual(n_grams, LanguageModel.generate_n_grams(the_doc, 2))

    def test_creating_a_language_model(self):
        LanguageModel('english')


    def test_getting_markov_probabilities(self):
        n_gram_model = {}

        for n in range(1,4):
            n_gram_model[n] = LanguageModel.generate_n_grams("the dog walks there docilly",n)

        perplexity_model = PerplexityModel()
        self.assertAlmostEqual(1.0,perplexity_model._get_markov_probability(n_gram_model,"th"))
        self.assertAlmostEqual(1/3,perplexity_model._get_markov_probability(n_gram_model,"er"))

    def test_getting_chain_probabilities(self):
        n_gram_model = {}

        for n in range(1, 4):
            n_gram_model[n] = LanguageModel.generate_n_grams("the dog walks there docilly", n)

        perplexity_model = PerplexityModel()
        self.assertAlmostEqual(2/81, perplexity_model._get_chained_probability(n_gram_model, "her"))

    def test_getting_chain_probability_ngram_model(self):
        n_gram_model = {}

        for n in range(1, 4):
            n_gram_model[n] = LanguageModel.generate_n_grams("the dog walks there docilly", n)

        perplexity_model = PerplexityModel()
        n_gram_probabilities = perplexity_model.generate_chained_probability(n_gram_model,3)

        self.assertAlmostEqual(2 / 81, n_gram_probabilities["her"])
