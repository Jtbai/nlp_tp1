from utils import generate_file_field_mapping,get_file_data_path
from wrf import get_wfb_info


def generate_mapping():
    file_list = get_file_data_path(HTML_DOCS_LOCATION, prefix=PRINT_DOCS_PREFIX)
    generate_file_field_mapping(HTML_DOCS_LOCATION,file_list,'field_mapping.csv')


def test_official_csv():
    with open('./test/wfb_test.csv', 'r') as f:
        total_nb_fit = 0
        total_tests_cases = 0
        for line in f:
            total_tests_cases += 1
            line = line.split(";")
            try:
                value = get_wfb_info(line[0], line[1])
                if line[2].strip() == value:
                    print("+++++ {0} = {1} ".format(value, line[2]))
                    total_nb_fit += 1
                else:
                    print("----- {0} = {1} ({2}, {3})".format(value, line[2], line[0], line[1]))

            except Exception as e:
                print(e)
                print("----- {0} ({1}, {2})".format(line[2], line[0], line[1]))

        print("Accuracy report : {0}%".format(total_nb_fit / total_tests_cases * 100))


test_official_csv()