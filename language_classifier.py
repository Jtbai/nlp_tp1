from language_model import LanguageModel
from perplexity_model import NoSmootingProbabilityModel, LaPlaceSmoothingProbabilityModel, LinearSmoothing
from numpy import argmin

languages = ['english','french','portuguese','espanol']
language_models = [LanguageModel(x) for x in languages]

truth_table = ["espanol",
"french",
"french",
"english",
"english",
"french",
"espanol",
"espanol",
"english",
"french",
"espanol",
"french",
"french",
"english",
"espanol",
"english",
"english",
"french",
"espanol",
"french"]

accuracy = []

#### Selection de la longueur du N-Gram
n_gram_length = 1

#### Selection du modele de calcul des probabilites

# perplexity_model = LinearSmoothing({3:1,2:0.75,1:0.00})
perplexity_model = LaPlaceSmoothingProbabilityModel(0.001)
# perplexity_model = NoSmootingProbabilityModel()


nb_good = 0
for i in range(1,21):

    text_to_test = LanguageModel.prepare_text('test'+str(i),'test',n_gram_length)
    perplexity = [perplexity_model.generate_perplexity(x,text_to_test,n_gram_length) for x in language_models]
    detected_language = languages[argmin(perplexity)]
    if detected_language == truth_table[i-1]:
        nb_good += 1
    else:
        print("Error : file {} ({}) deteced as {}".format(i,truth_table[i-1],detected_language))

print("Accuracy : {}".format(nb_good/20))

