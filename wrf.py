from utils import convert_selection_string_to_field_file, get_full_file_text, convert_selection_string_field_selector

HTML_DOCS_LOCATION = './data/factbook/fields'
PRINT_DOCS_PREFIX = 'print_'


def get_wfb_info(pays, attribut):
    field_file = convert_selection_string_to_field_file(attribut)
    html_doc = get_full_file_text(HTML_DOCS_LOCATION, field_file)

    field = convert_selection_string_field_selector(attribut)(html_doc)
    return field.get_value(pays)

