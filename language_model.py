from os.path import join

DOCS_LOCATION = './data/language_texts'
TRAINING_LOCATION_SUFFIX = 'corpus_entrainement'
TEST_LOCATION_SUFFIX = 'corpus_test'
TRAINING_DOCS_SUFFIX = '-training.txt'

class LanguageModel:

    pre_process_pipeline = {'remove': ['\'','\n','(',')','\"'], 'to_lower': 1}

    def __init__(self, language):
        self.n_gram_model = {}
        self.language = language
        training_text = LanguageModel.document_loader(language,'train')
        self.training_text = LanguageModel.pre_process_document(training_text,self.pre_process_pipeline)
        for n in range(3):
            self.n_gram_model[n+1] = LanguageModel.generate_n_grams(self.training_text,n+1)

    def __repr__(self):
        return self.language

    @staticmethod
    def document_loader(name, type):
        try:
            if type == "train":
                with open(join(DOCS_LOCATION, TRAINING_LOCATION_SUFFIX, "{}{}".format(name, TRAINING_DOCS_SUFFIX)),
                          'r') as f:
                    return f.read()
            if type == "test":
                with open(join(DOCS_LOCATION, TEST_LOCATION_SUFFIX, "{}{}".format(name, '.txt')), 'r') as f:
                    return f.read()

        except FileNotFoundError:
            raise FileNotFoundError("Le fichier demande n'est pas disponible")

    @staticmethod
    def pre_process_document(text_document, pipeline):
        for treatment in pipeline.items():
            key, value = treatment

            if key.lower() == "remove":
                for value_to_consider in value:
                    text_document = text_document.replace(value_to_consider, '')
            if key.lower() == "to_lower":
                text_document = text_document.lower()

        return text_document

    @staticmethod
    def generate_n_grams(text_document, n):

        n_grams = {}
        for index in range(len(text_document) - n + 1):
            n_gram = text_document[index:index + n]
            n_grams[n_gram] = n_grams.get(n_gram, 0) + 1

        return n_grams

    @staticmethod
    def prepare_text(file, type, n):
        doc = LanguageModel.document_loader(file,type)
        pre_process_doc = LanguageModel.pre_process_document(doc,LanguageModel.pre_process_pipeline)
        n_gram = LanguageModel.generate_n_grams(pre_process_doc,n)
        return n_gram



