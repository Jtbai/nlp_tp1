import re

class Field:

    source = None
    country_source = None

    beggining_token = None
    extraction_token = None
    end_token = None

    def __init__(self, html_document):
        self.source = html_document

    def _get_country_sub_document(self, country):
        country_extraction_token = re.search("({0}<\\/a>)[\\s\\S]+?(_region)".format(country), self.source, re.IGNORECASE)

        if country_extraction_token:
            self.country_source = self.source[country_extraction_token.start(1):country_extraction_token.end(2)]
        else:
            raise Exception("le pays n'est pas dans la liste")

    def _extract_field_value(self):
        reg = re.compile("{0}{1}{2}".format(self.beggining_token, self.extraction_token, self.end_token))
        matching = reg.search(self.country_source,re.IGNORECASE)
        return self._generate_output_value(matching)

    def _generate_output_value(self, matching):
        return matching.group(1)

    def get_value(self, country):
        self._get_country_sub_document(country)
        return self._extract_field_value()

class NationalAnthem(Field):

    beggining_token = "<strong>name: <\/strong>\""
    extraction_token = "([A-z ,\\-]+)\" \\(?([A-z ,\\-]+)?\\)?"
    end_token = ""

    def _generate_output_value(self,matching):
        if matching.group(2):
            return "{0}".format(matching.group(2))
        else:
            return "{0}".format(matching.group(1))

class Literacy(Field):

    beggining_token = "<strong>male:</strong> "
    extraction_token = "([0-9.]+)"
    end_token = ""

    def _generate_output_value(self,matching):
        return "{0}%".format(matching.group(1))

class Exports(Field):

    beggining_token = "\\$"
    extraction_token = "([0-9.]+) ((?:b|m|tr)illion)"
    end_token = ""

    def _generate_output_value(self, matching):
        if matching:
            unit = matching.group(2)
            value = matching.group(1)

            if unit == "million":
                complete_value = int(float(value) * 1e6)
            elif unit == "billion":
                complete_value = int(float(value) * 1e9)
            elif unit == "trillion":
                complete_value = int(float(value) * 1e12)
            else:
                raise Exception("Unknown monetary unit: {0}".format(unit))

            return "${0}".format(complete_value)

class GdpRealGrowthRate(Field):

    beggining_token = ""
    extraction_token = "(-?[0-9.]+)"
    end_token = "%"

    def _generate_output_value(self, matching):
        return "{0}%".format(matching.group(1))

class GdpPerCapita(Field):
    beggining_token = "\\$"
    extraction_token = "([0-9,]+)"
    end_token = ""

    def _extract_field_value(self):
        reg = re.compile("{0}{1}{2}".format(self.beggining_token, self.extraction_token, self.end_token))
        matching = reg.findall(self.country_source)
        return self._generate_output_value(matching)

    def _generate_output_value(self, matching):

        return "${0}".format(max([int(x.replace(',','')) for x in matching]))

class NaturalHazards(Field):

    beggining_token = ""
    extraction_token = "(wind|cyclon|typhoon)"
    end_token = ""

    def _generate_output_value(self, matching):
        outcome = "NO"
        if matching:
            outcome = "YES"

        return outcome

class ExecutiveBranch(Field):

    beggining_token = "<strong>(?:chief|head) of state: <\\/strong>"
    extraction_token = "(?:French )?(Interim |)([A-Za-z\\-]+) ([A-z -]+)"
    end_token = " \\("

    def _generate_output_value(self, matching):

        return "{0}, {1}{2}".format(matching.group(3), matching.group(1), matching.group(2))

class DiplomaticRepresentationFromUs(Field):
    beggining_token = "<strong>mailing address:<\\/strong> "
    extraction_token = "([a-zA-Z0-9_ ,.]+)"
    end_token = ""

    def _generate_output_value(self, matching):


        if matching:
            matching_address = self._get_full_address(matching.group(1))
        else:
            return "No_diplomatic_rep"

        if matching_address:
            return matching_address.group(1).strip()
        else:
            matching_po_box = self._get_po_box(matching.group(1))
            if matching_po_box:
                return matching_po_box.group(1).strip()
            else:
                return "No_diplomatic_rep"

    def _get_po_box(self, matched_string):
        po_box_patern = '((?:[A-Z](?:.)*)*[0-9]*(?:(?:, )?[bB]ox)? [0-9]+, \w+)'

        return re.search(po_box_patern, matched_string, re.IGNORECASE)

    def _get_full_address(self, matched_string):
        po_box_patern = '([0-9]+ [\w ]+, [\w ]+)'

        return re.search(po_box_patern, matched_string, re.IGNORECASE)




